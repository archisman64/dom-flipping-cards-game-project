const fruitCards = document.querySelectorAll('.fruit-card');
const gameContainer = document.querySelector('.game-card'); 
const moveCounter = document.getElementById('moves');
const timer = document.querySelector('.timer');
const startBtn = document.querySelector('.start-btn');

console.log('fruit cards:', fruitCards);

const cardPair = [];
let matchedCount = 0;
let movesCount = 0;
let time = 0;
let timerId;
let firstClick = true;

gameContainer.addEventListener('click', (event) => {
    event.preventDefault();
    const clicked = event.target.className.split(' ')[0];
    if(clicked === 'fruit-card-front') {
        // console.log(event.target.parentElement.parentElement)
        const fruitCard = event.target.parentElement.parentElement;
        solver(fruitCard);
    }  
});

startBtn.addEventListener('click', (event) => {
    event.preventDefault();
    if(firstClick) {
        startBtn.classList.add('grey');
        startTimer();
        firstClick = false;
    }
})

function solver(fruitCard) {
    // starting the timer with the help of first click flag
    if(firstClick) {
        startBtn.classList.add('grey');
        startTimer();
        firstClick = false;
    }
    // adding active class list to show the fruit(back of the card)
    fruitCard.classList.add('active');
    fruitCard.classList.add('unclickable');
    // increment moves
    incrementMoves();

    setTimeout(() => {
        checkForMatchedCards(fruitCard);
    }, 1000)
}

function checkForMatchedCards(fruitCard) {
    cardPair.push(fruitCard);
    if(cardPair.length === 2) {
        // check for matched cards
        if(cardPair[0].children[0].firstElementChild.children[0].src === cardPair[1].children[0].firstElementChild.children[0].src) {
            console.log('matched!');

            // make the cards disappear
            cardPair[0].style.visibility = 'hidden';
            cardPair[1].style.visibility = 'hidden';
            // increament matchedCount
            matchedCount ++;

            if(matchedCount === 8) {
                // end the game
                confirm(`Congratulations! You completed the game in ${time} sec.`);
                clearInterval(timerId);
                return;
            }
        } else {
            // if the cards don't match, make them clickable again
            cardPair[0].classList.remove('unclickable');
            cardPair[1].classList.remove('unclickable');
        }
        cardPair[0].classList.remove('active');
        cardPair[1].classList.remove('active');
       
        cardPair.pop();
        cardPair.pop();
        return;
    }
}

function incrementMoves() {
    movesCount += 1;
    moveCounter.innerText = `${movesCount} moves`;
}

function startTimer() {
    timerId = setInterval(() => {
        time += 1;
        timer.innerText = `time: ${time} sec`;
    }, 1000);
}